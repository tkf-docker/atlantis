# docker-atlantis

This container image is derived from the runatlantis.io container.

It includes software like Terragrunt and terragrunt-atlantis-config.

Terragrunt is installed through the tgenv utility.

