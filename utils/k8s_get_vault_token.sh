#!/usr/bin/env bash

TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
VAULT_TOKEN=$(curl -s --request POST --data '{"jwt": "'"$TOKEN"'", "role": "atlantis"}' \
  ${VAULT_ADDR}/v1/auth/kubernetes/login | jq -r .auth.client_token)

echo $VAULT_TOKEN