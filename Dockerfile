ARG ATLANTIS_VERSION
FROM ghcr.io/runatlantis/atlantis:v${ATLANTIS_VERSION}
LABEL maintainer="teknofile"

ARG TARGETPLATFORM

ARG TG_ATLANTIS_CONFIG_VER
ARG TG_VERSION

RUN apk update && apk add \
  jq

RUN git clone https://github.com/cunymatthieu/tgenv.git /opt/tgenv && \
  ln -s /opt/tgenv/bin/* /usr/local/bin/ && \
  tgenv install ${TG_VERSION} && \
  tgenv use ${TG_VERSION} && \
  chown -R atlantis:root /opt/tgenv

# TODO: This is not cross-platform-aware
RUN echo "tfatlantisconfig ver is $TG_ATLANTIS_CONFIG_VER" && \
  curl -sL https://github.com/transcend-io/terragrunt-atlantis-config/releases/download/v${TG_ATLANTIS_CONFIG_VER}/terragrunt-atlantis-config_${TG_ATLANTIS_CONFIG_VER}_linux_amd64.tar.gz -o /tmp/terragrunt-atlantis-config_${TG_ATLANTIS_CONFIG_VER}_linux_amd64.tar.gz && \
  tar xf /tmp/terragrunt-atlantis-config_${TG_ATLANTIS_CONFIG_VER}_linux_amd64.tar.gz -C / && \
  mv /terragrunt-atlantis-config_${TG_ATLANTIS_CONFIG_VER}_linux_amd64/terragrunt-atlantis-config_${TG_ATLANTIS_CONFIG_VER}_linux_amd64 /terragrunt-atlantis-config_${TG_ATLANTIS_CONFIG_VER}_linux_amd64/terragrunt-atlantis-config && \
  install /terragrunt-atlantis-config_${TG_ATLANTIS_CONFIG_VER}_linux_amd64/terragrunt-atlantis-config /usr/local/bin

COPY utils/* /usr/local/bin/

RUN chmod +x /usr/local/bin/k8s_get_vault_token.sh
